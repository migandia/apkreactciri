import React, { useEffect, useState } from "react";
import { StyleSheet } from "react-native"
import {
  Text,
  HStack,
  Heading,
  VStack,
  Box,
  FlatList,
  Button,
  Stack,
  ScrollView,
  Flex,
  Divider,
} from "native-base";
import NativeBaseIcon from "../components/NativeBaseIcon";
import { getDiagnosticos } from "../api";

const image = { uri: "https://reactjs.org/logo-og.png" };

export default function App({ navigation }) {
  const [data, setdata] = useState([])

  const loadDiag = async () => {
    const data = await getDiagnosticos()
    setdata(data)
  }

  const p = useEffect(async () => {
    loadDiag()
  }, [])

  return (
    <Box h="100%" w="100%">
      <Heading
        borderBottomWidth="1"
        _dark={{
          borderColor: "gray.600"
        }}
        borderColor="coolGray.200"
        pl="4"
        pr="4"
        textAlign="center"
      >
        SELECCIONE EL DIAGNOSTICO ADECUADO
      </Heading>
      <FlatList
        data={data}
        renderItem={
          ({ item }) =>
            <Box
              borderBottomWidth="1"
              _dark={{
                borderColor: "gray.600"
              }}
              borderColor="coolGray.200"
              pl="4"
              pr="5"
              py="5"
              style={styles.caja}
            >
              <HStack space={3} justifyContent="space-around">
                <VStack>
                  <Text
                    _dark={{ color: "warmGray.50" }}
                    color="coolGray.800"
                    fontSize={17}
                    bold
                    style={styles.text}
                  >
                    {item.diagnostico}
                  </Text>
                  <Text
                    color="coolGray.600"
                    _dark={{ color: "warmGray.200" }}
                    fontSize={15}
                  >
                    {item.descripcion}
                  </Text>
                </VStack>
              </HStack>
              <Button onPress={() => navigation.navigate("Preguntas", { itemId: item.id })}>
                <Text color="white">SELECCIONAR</Text>
              </Button>
              <Divider my="2" _light={{
                bg: "muted.800"
              }} _dark={{
                bg: "muted.50"
              }} />
            </Box>
        }
        keyExtractor={item => item.id}
      />
    </Box>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    justifyContent: "center"
  },
  text: {
    fontSize: 42,
    lineHeight: 84,
    fontWeight: "bold",
    textAlign: "center",
  },
  caja: {
    borderColor: "green",
    borderWidth: 2,
    backgroundColor: "#ecfdf5",
    marginLeft: 5,
    marginRight: 5,
  }
});