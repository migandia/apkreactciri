import React, { useEffect, useState } from "react";
import {
  Text,
  HStack,
  Heading,
  VStack,
  Box,
  FlatList,
  Button,
  Stack,
  ScrollView,
  Flex,
  Checkbox,
} from "native-base";
import NativeBaseIcon from "../components/NativeBaseIcon";
import { getCuestionario, saveTask1, getTask1 } from "../api";
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function Cuestionario({ route, navigation }) {
  const [data, setdata] = useState([])
  const [groupValues, setGroupValues] = React.useState([]);
  const { itemId } = route.params
  const [salaChat, setSalaChat] = React.useState([]);

  const loadCuestionario = async () => {
    const data = await getCuestionario(itemId)
    setdata(data)
  }

  const p = useEffect(async () => {
    loadCuestionario()
    const espera = await AsyncStorage.getItem("id")
    handleChange('paciente_chat_estado', espera);
    const yase = await getTask1(espera, 1, true);
    setSalaChat(yase)
  }, [])


  const handleChange = (name, value) => setTask({ ...task, [name]: value });

  const [task, setTask] = useState({
    paciente_chat_estado: '',
    estados_chat: '1',
    activo: 'true'
  });

  const enviarRespuestas = async () => {
    if (Object.keys(groupValues).length >= 2) {
      if (Object.keys(salaChat).length == 1) { } else {
        const id = await AsyncStorage.getItem("id")
        handleChange("paciente_chat_estado", id)
        await saveTask1(task);
      }
      const textRespuesta = groupValues.join()
      navigation.navigate("Chat", { textRespuesta: textRespuesta, itemId: itemId })
    } else {
      alert("Según a sus respuestas no es necesario que pase a consultar con un Terapeuta. Revise las siguiente información. Gracias por comunicarse con nosotros.")
      navigation.navigate("InfAdicional")
    }
  }

  return (
    <Box h="100%" w="100%">
      <VStack space={2.5} w="100%" mt="4">
        <Heading
          borderBottomWidth="1"
          _dark={{
            borderColor: "gray.600"
          }}
          borderColor="coolGray.200"
          pl="4"
          pr="4"
          textAlign="center"
        >
          Responda con toda la sinceridad
        </Heading>
        <FlatList
          data={data}
          renderItem={
            ({ item }) =>
              <Box
                borderBottomWidth="1"
                _dark={{
                  borderColor: "gray.600"
                }}
                borderColor="coolGray.200"
                pl="4"
                pr="5"
                py="5"
              >
                <HStack space={3} justifyContent="space-between">
                  <VStack>
                    <Checkbox.Group onChange={setGroupValues} value={groupValues} accessibilityLabel="choose numbers">
                      <Checkbox
                        box value={item.id} my={2} colorScheme="info">
                        <Text
                          _dark={{ color: "warmGray.50" }}
                          color="coolGray.800"
                          fontSize={15}
                          bold
                        >
                          {item.pregunta}
                        </Text>
                      </Checkbox>
                    </Checkbox.Group>
                  </VStack>
                </HStack>
              </Box>
          }
          keyExtractor={item => item.id}
        />
      </VStack>
      <Button colorScheme="secondary" onPress={enviarRespuestas}>
        <Text color="white">ENVIAR RESPUESTAS</Text>
      </Button>
    </Box>
  );
}
