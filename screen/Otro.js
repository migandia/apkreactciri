import { ImageBackground, StyleSheet, Text, View, Box } from 'react-native'
import React from 'react'
import { Stack, VStack, Image } from 'native-base';

// const image = { uri: "https://reactjs.org/logo-og.png" };
const image = require('../assets/TDAH.jpg');

const Otro = () => {
  return (
    <VStack style={styles.container} alignItems="center">
      <Image source={require('../assets/TDAH.jpg')}  width={200} height={200} />
    </VStack>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    justifyContent: "center"
  },
  text: {
    color: "white",
    fontSize: 42,
    lineHeight: 84,
    fontWeight: "bold",
    textAlign: "center",
    backgroundColor: "#000000c0"
  }
});

export default Otro