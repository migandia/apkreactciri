import { View, Text, TextInput, StyleSheet, TouchableOpacity,Center,ScrollView,Heading } from 'react-native';
import React, { useState,useEffect } from 'react';

import Layout from '../components/Layout';
import { saveTask, getTask,updateTask } from '../api';

const TaskFormScreen = ({ navigation, route }) => {

  const [task, setTask] = useState({
    nombre: '',
    ap_paterno: '',
    ap_materno: '',
    matricula: '',
    ci: '',
    telefono: '',
    fecha_nacimiento: ''
  });

  const [editing, setEditing] = useState(false);
  const handleChange = (name, value) => setTask({ ...task, [name]: value });

  const handleSubmit = async () => {
    try{
      if (!editing){
        await saveTask(task);      
      }else{
        await updateTask(route.params.id, task);
      }
      navigation.navigate('Login');

    }catch (error){
      console.error(error);
    }
    
  };

  useEffect(() => {
    if(route.params && route.params.id){
      navigation.setOptions({headerTitle: 'Actualizar el registro'});
      setEditing(true);
    (async() => {
      const  task =  await getTask(route.params.id);
      setTask({nombre:task.nombre,ap_paterno:task.ap_paterno,ap_materno:task.ap_materno,matricula: task.matricula, ci: task.ci, telefono: task.telefono,fecha_nacimiento: task.fecha_nacimiento});
    })();
    }
  },[]);

   return (
   /*  <Center mt="3" mb="4">
    <Heading fontSize="xl">Registro del paciente </Heading>
    <Heading fontSize="xl">para atención psicológica </Heading>
  </Center> */
    <Layout>
      <TextInput
        style={styles.input}
        placeholder='Nombre'
        placeholderTextColor="#576574"
        onChangeText={(text) => handleChange('nombre', text)}
        value={task.nombre}
       />
      <TextInput
        style={styles.input}
        placeholder='Apellido Paterno'
        placeholderTextColor="#576574"
        onChangeText={(text) => handleChange('ap_paterno', text)}
        value={task.ap_paterno}
      />
      <TextInput
        style={styles.input}
        placeholder='Apellido Materno'
        placeholderTextColor="#576574"
        onChangeText={(text) => handleChange('ap_materno', text)}
        value={task.ap_materno}
      />
      <TextInput
        style={styles.input}
        placeholder='Matrícula'
        placeholderTextColor="#576574"
        onChangeText={(text) => handleChange('matricula', text)}
        value={task.matricula}
      />
       <TextInput
        style={styles.input}
        placeholder='C.I'
        placeholderTextColor="#576574"
        onChangeText={(text) => handleChange('ci', text)}
        value={task.ci}
      />
       <TextInput
        style={styles.input}
        placeholder='Celular'
        placeholderTextColor="#576574"
        onChangeText={(text) => handleChange('telefono', text)}
        value={task.telefono}
      />
        <TextInput
        style={styles.input}
        placeholder='Fecha de nacimiento(año-mes-día) : (Ej 2010-09-21)'
        placeholderTextColor="#576574"
        onChangeText={(text) => handleChange('fecha_nacimiento', text)}
        value={task.fecha_nacimiento}
      />
      {
        !editing ?(
          <TouchableOpacity style={styles.buttonSave} onPress={handleSubmit}>
          <Text style={styles.buttonText}>Guardar</Text>
        </TouchableOpacity>  
        ):(
          <TouchableOpacity style={styles.buttonUpdate} onPress={handleSubmit}>
          <Text style={styles.buttonText}>Actualizar</Text>
        </TouchableOpacity>
        )}   
    </Layout>
  )
} 
  const styles = StyleSheet.create({
    input: {
      width: '90%',
      marginBottom: 7,
      fontSize: 14,
      borderWidth: 1,
      borderColor: '#008000',
      height: 35,
      color: '#000000',
      textAlign: 'center',
      borderRadius: 5,
      padding: 4
    },
    buttonSave: {
      paddingTop: 10,
      paddingBottom: 10,
      borderRadius: 5,
      marginBottom: 10,
      backgroundColor: '#008000',
      width: '90%',
    },
    buttonText: {
      color: '#ffffff',
      textAlign: 'center'
    },
    buttonUpdate: {
      padding: 10,
      paddingBottom: 10,
      borderRadius: 5,
      marginBottom: 3,
      backgroundColor: "#008000",
      width: "90%"
    }
  })
  
export default TaskFormScreen