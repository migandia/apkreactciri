import React, { useEffect, useState } from "react";
import {
  Text,
  Heading,
  VStack,
  Box,
  FlatList,
  Button,
  Stack,
} from "native-base";
import NativeBaseIcon from "../components/NativeBaseIcon";
import { getInfAdicional, getSalaChat, getTask1 } from "../api";
/** Sotorage */
import AsyncStorage from '@react-native-async-storage/async-storage';
/** FIN */

export default function App({ navigation }) {
  const [data, setdata] = useState([])

  const loadTask = async () => {
    const data = await getInfAdicional()
    setdata(data)
  }

  const p = useEffect(async () => {
    const prueba = await AsyncStorage.getItem('nombre')
    loadTask()
  }, [])

  const verificarEstado = async () => {
    const id = await AsyncStorage.getItem("id")
    const estado = await getSalaChat(id)
    if (Object.keys(estado).length == 1) {
      AsyncStorage.setItem("chat", estado[0].id + '');
      navigation.navigate("Chat", { textRespuesta: '', itemId: '' })
    } else {
      navigation.navigate("Diagnostico")
    }
  }

  return (
    <Box h="100%" w="100%">
      <Heading
        borderBottomWidth="1"
        _dark={{
          borderColor: "gray.600"
        }}
        borderColor="coolGray.200"
        pl="4"
        pr="4"
        textAlign="center"
      >
        Antes de continuar con la atención, lea la siguiente Información
      </Heading>
      <FlatList
        data={data}
        renderItem={
          ({ item }) =>
            <Box
              borderBottomWidth="1"
              _dark={{
                borderColor: "gray.600"
              }}
              borderColor="coolGray.200"
              pl="4"
              pr="5"
              py="5"
            >
              <Stack space={3} justifyContent="space-between">
                <VStack>
                  <Text
                    _dark={{ color: "warmGray.50" }}
                    color="coolGray.800"
                    fontSize={17}
                    bold
                  >
                    {item.pregunta}
                  </Text>
                  <Text
                    color="coolGray.600"
                    _dark={{ color: "warmGray.200" }}
                    fontSize={15}
                  >
                    {item.descripcion}
                  </Text>
                </VStack>
              </Stack>
            </Box>
        }
        keyExtractor={item => item.id}
      />
      <Button onPress={() => verificarEstado()}>
        <Text color="white">CONTINUAR</Text>
      </Button>
    </Box>
  );
}
