import React, { useEffect, useState } from "react"
import { StyleSheet, TextInput } from "react-native";
import {
  Text,
  HStack,
  VStack,
  Box,
  FlatList,
  Button,
  View,
  Spinner,
  Heading
} from "native-base";
import NativeBaseIcon from "../components/NativeBaseIcon";
import AsyncStorage from '@react-native-async-storage/async-storage'
import { getMensages, getSalaChat, getRespuestas, saveMessages, saveRespuestas } from '../api'

export default function Cuestionario({ route, navigation }) {
  const [mensaje, setMensaje] = useState([])
  let internal
  const [m, setm] = useState('');
  const { textRespuesta } = route.params
  const { itemId } = route.params
  const [shouldShow, setShouldShow] = useState(true)
  const [idDoctor, setidDoctor] = useState()

  const p = useEffect(() => {
    internal = setInterval(() => {
      listaMensajes()
    }, 4000)

    return () => clearInterval(internal)
  }, [])

  const handleChange = (name, value) => setm({ ...m, [name]: value });

  const enviar = async () => {
    const id = await AsyncStorage.getItem("id")
    const chat = await AsyncStorage.getItem("chat")

    if (m != '') {
      handleChange('mensaje', m)
      saveMessages(chat, id, idDoctor, m)
    } else {
      alert("Escriba el mensaje, a ser enviado.")
    }
    setm('')
  }

  const listaMensajes = async () => {
    const id = await AsyncStorage.getItem("id")
    const chat = await AsyncStorage.getItem("chat")
    if (chat != null) {
      const data = await getMensages(chat, id)
      const data1 = await getSalaChat(id)
      if (Object.keys(data1).length > 0) {
        setidDoctor(data1[0].users_permissions_user.id + '')
        setMensaje(data)
        setShouldShow(false)
      } else {
        alert("Gracias por comunicarse con nosotros. No dude en hacernos llegar sus consultas.")
        navigation.navigate("Login")
      }

      // setidDoctor(data1[0].users_permissions_user.id + '')
      // setMensaje(data)
      // setShouldShow(false)
    } else {
      setShouldShow(true)
      const data = await getSalaChat(id)
      if (Object.keys(data).length == 1) {
        AsyncStorage.setItem('chat', data[0].id + '')
        setidDoctor(data[0].users_permissions_user.id + '')
        const resGuardada = await getRespuestas(id)
        if (Object.keys(resGuardada).length == 1) {

        } else {
          const res = await saveRespuestas(textRespuesta, data[0].id, itemId)
        }
      }
    }
  }

  return (
    <Box h="100%" w="100%">
      {shouldShow ? (
        <View height="100%" style={{ alignItems: "center" }}>
          <Spinner accessibilityLabel="Loading posts" size="lg" style={{ marginTop: "60%" }} />
          <Heading fontSize="xl">
            <Text>Espere hasta que un Terapeuta de CIRI acepte el Chat.</Text>
          </Heading>
        </View>
      ) : (
        <Box h="100%" w="100%">
          <FlatList
            data={mensaje}
            renderItem={
              ({ item }) =>
                <Box
                  borderBottomWidth="1"
                  _dark={{
                    borderColor: "gray.600"
                  }}
                  borderColor="coolGray.200"
                  pl="4"
                  pr="5"
                  py="5"
                  style={{
                    alignItems: item.tipo == 'P' ? 'flex-end' : 'flex-start', backgroundColor: item.tipo == 'P' ? "#0891b2" : "#22d3ee",
                    paddingVertical: 16,
                    paddingHorizontal: 12,
                    borderRadius: 20,
                    alignSelf: 'center',
                    width: 375,
                    fontWeight: 'bold',
                    maxWidth: '100%'
                  }}
                >
                  <HStack space={3} justifyContent="space-between">
                    <VStack>
                      <Box p="1" borderRadius="md">
                        <Text
                          _dark={{ color: "warmGray.50" }}
                          color="coolGray.800"
                          fontSize={15}
                          bold
                          style={{ color: "#ffffff" }}
                        >{item.mensaje}</Text>
                      </Box>
                      <Box>
                        <Text style={{ fontSize: 11, color: "#ffffff" }}>
                          {
                            item.tipo == 'P' ?
                              item.paciente.nombre + ' ' + item.paciente.ap_paterno + ' ' + item.paciente.ap_materno + '\n' +
                              new Date(item.created_at).getDate() + '/' + (new Date(item.created_at).getMonth() + 1) + '/' + new Date(item.created_at).getFullYear()
                              :
                              item.users_permissions_user.nombre + ' ' + item.users_permissions_user.ap_paterno + ' ' + item.users_permissions_user.ap_materno + '\n' +
                              new Date(item.created_at).getDate() + '/' + (new Date(item.created_at).getMonth() + 1) + '/' + new Date(item.created_at).getFullYear()}</Text>
                      </Box>
                    </VStack>
                  </HStack>
                </Box>
            }
            keyExtractor={item => item.id}
          />
          <View style={styles.mensajeContainer}>
            <TextInput
              style={styles.input}
              onChangeText={(text) => setm(text)}
              defaultValue={m + ''}
            ></TextInput>
            <Button style={styles.btn} onPress={enviar}>
              <Text style={{ color: "#ffffff" }}>Enviar</Text>
            </Button>
          </View>
        </Box>
      )}
    </Box>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  mensajeContainer: {
    flexDirection: "row",
    justifyContent: 'space-between'
  },
  input: {
    width: '80%',
    marginBottom: 7,
    fontSize: 14,
    borderWidth: 1,
    borderColor: '#008000',
    height: 35,
    color: '#000000',
    textAlign: 'center',
    borderRadius: 5,
    padding: 4
  },
  btn: {
    width: '20%',
    marginBottom: 7,
    marginLeft: 2,
  },
})