import * as React from "react";
import { ScrollView,CloseIcon,Alert, Collapse,Box, Text, Heading, VStack, FormControl, Input, Link, Button, HStack, Center, NativeBaseProvider,Icon,IconButton,Image,View,StatusBar,ArrowForwardIcon, AlertDialog } from "native-base";
import { AntDesign } from "@expo/vector-icons";
import { getTask } from '../api';
import { useState,useEffect } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';


  export default function App({ navigation }) {
    const p = useEffect(async () => {
      setShow(false);
    }, []);
    
    const handleSubmit = async () => {    
      try{     
        if (task.ci != '' && task.matricula !=''){   
        const retorno  =await getTask(task.ci,task.matricula);
        if (Object.keys(retorno).length == 1){
          AsyncStorage.setItem("id",retorno[0].id+'');
          AsyncStorage.setItem("nombre",retorno[0].nombre + retorno[0].ap_paterno + retorno[0].ap_materno);
          AsyncStorage.setItem("matricula",retorno[0].matricula);
          AsyncStorage.setItem("ci",retorno[0].ci);
          AsyncStorage.setItem("chat",'');
          navigation.navigate('InfAdicional');
        }else{
         
          setShow(true);
        }
        }else{
          alert('Debe llenar los campos : Matrícula y C.I.'); 
        }
  
      }catch (error){
        console.error(error);
      }
      
    };

    const [task, setTask] = useState({
      matricula: '',
      ci: '',
      });
      const handleChange = (name, value) => setTask({ ...task, [name]: value });
      /* Para mostrar la alerta */
      const [show, setShow] = React.useState(true);
      /*-----------------------*/
  return (<ScrollView> 
    <Center w="100%">
 
<View >
   <Image
     style={{ width: 120, height: 120, marginBottom: 35 }}
     source={require("../assets/logoCNS.png")} alt="Caja Nacional de Salud"
   />            
</View>
{/*Para mostrar la alerta */}
 <Box w="100%" alignItems="center">
      <Collapse isOpen={show}>
        <Alert maxW="400" status="warning">
          <VStack space={1} flexShrink={1} w="100%">
            <HStack flexShrink={1} space={2} alignItems="center" justifyContent="space-between">
              <HStack flexShrink={1} space={2} alignItems="center">
                <Alert.Icon />
                <Text fontSize="md" fontWeight="medium" _dark={{
                color: "coolGray.800"
              }}>
                  Usted no se encuentra registrado. Por favor apersonese a su Centro de Salud Asignado. Para el Registro.
                </Text>
              </HStack>
              <IconButton variant="unstyled" _focus={{
              borderWidth: 0
            }} icon={<CloseIcon size="3" />} _icon={{
              color: "coolGray.600"
            }} onPress={() => setShow(false)} />
            </HStack>
          </VStack>
        </Alert>
      </Collapse>
     
    </Box> 

{/*--------------------------------------------------------*/}
 <Box safeArea p="2" py="8" w="90%" maxW="290">
   <Heading size="lg" fontWeight="600" color="coolGray.800" _dark={{
   color: "warmGray.50"
 }}>
     Atención Psicológica
   </Heading>
   <Heading mt="1" _dark={{    
   color: "warmGray.200"
 }} color="coolGray.600" fontWeight="medium" size="xs">
     Ingrese para ser atendido:
   </Heading>

   <VStack space={3} mt="5">
     <FormControl >
       <FormControl.Label>Matrícula (AVC)</FormControl.Label>
       <Input 
        onChangeText={(text) => handleChange('matricula', text)}
       value={task.matricula} 
        />
     </FormControl>
     <FormControl>
       <FormControl.Label>Carnet de Identidad</FormControl.Label>
       <Input 
        onChangeText={(text) => handleChange('ci', text)}
       value={task.ci} 
       />
     </FormControl>
       <Button colorScheme="green"  onPress={ handleSubmit} > 
          <Text color="white">Ingresar</Text>
    </Button> 
      
    {/*  <HStack mt="6" justifyContent="center">
       <Text fontSize="sm" color="coolGray.600" _dark={{
       color: "warmGray.200"
     }}>
         ¿No está registrado?
        </Text>             
       </HStack>
       <HStack mt="6" justifyContent="center">
       <Text fontSize="sm" color="coolGray.600" _dark={{color: "warmGray.200"}}>Haga click aqui -- </Text> 
       {["outline" ].map(variant => <IconButton colorScheme="green" key={variant} variant={variant} _icon={{as: AntDesign,name: "edit"}} onPress={() => navigation.navigate("Registro del Paciente")}/>)}
       
       </HStack>
       <HStack mt="6" justifyContent="center">
    
     </HStack> */}
   </VStack>
   <View >
   <Image
     style={{ width:273 , height: 120, marginBottom: 35 }}
     source={require("../assets/logo_ciri.png")} alt="C.I.R.I"
   />            
</View>
<Text fontSize="sm" color="coolGray.900" _dark={{
       color: "warmGray.400"
     }}>
      Desarrollado por:    
</Text>
<Text fontSize="sm" color="coolGray.900" _dark={{
       color: "warmGray.400"
     }}>
         Unidad de Sistemas - CNS Regional La Paz
</Text>
 </Box>
    

</Center>
</ScrollView> 
)}

//export default HomeScreen
/* export default () => {
  return (
    <NativeBaseProvider>
      <Center flex={1} px="3">
          <HomeScreen />
      </Center>
    </NativeBaseProvider>
  );
};
 */