import { React } from "react"
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
/** template */
import {
  Text,
  Link,
  HStack,
  Center,
  Heading,
  Switch,
  useColorMode,
  NativeBaseProvider,
  extendTheme,
  VStack,
  Box,
  View,
  Button,
} from "native-base";
import { Platform, } from "react-native";
import NativeBaseIcon from "./components/NativeBaseIcon";
/** template */

/** Archivos importados */

import Diagnostico from './screen/Diagnostico'
import Preguntas from './screen/Preguntas'
import Otro from './screen/Otro'

import Login from './screen/Login';
import Reg_paciente from './screen/Reg_paciente';
import Inf_adicional from './screen/Inf_adicional';
import Chat from './screen/Chat';

/** Archivos importados */

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <NativeBaseProvider>
        <Stack.Navigator>
          <Stack.Screen
            name="Login"
            component={Login}
            options={{
              title: '  Centro Integral de Rehabilitación Infantil',
              headerStyle: { backgroundColor: "green" },
              headerTitleStyle: { color: "#ffffff" },
              headerTintColor: '#ffffff'
            }}
          />
          <Stack.Screen name="InfAdicional" component={Inf_adicional}
            options={{
              title: 'INFORMACION ADICIONAL',
              headerStyle: { backgroundColor: "green" },
              headerTitleStyle: { color: "#ffffff" },
              headerTintColor: '#ffffff'
            }}
          />
          <Stack.Screen name="Diagnostico" component={Diagnostico}
            options={{
              title: 'DIAGNOSTICOS',
              headerStyle: { backgroundColor: "green" },
              headerTitleStyle: { color: "#ffffff" },
              headerTintColor: '#ffffff'
            }}
          />
          <Stack.Screen name="Preguntas" component={Preguntas}
            options={{
              title: 'CUESTIONARIO',
              headerStyle: { backgroundColor: "green" },
              headerTitleStyle: { color: "#ffffff" },
              headerTintColor: '#ffffff'
            }}
          />
          <Stack.Screen name="Chat" component={Chat}
            options={{
              title: 'CHAT',
              headerStyle: { backgroundColor: "green" },
              headerTitleStyle: { color: "#ffffff" },
              headerTintColor: '#ffffff'
            }}
          />


          <Stack.Screen
            name="Reg_paciente"
            component={Reg_paciente}
            options={{
              title: 'Registro del paciente',
              headerStyle: { backgroundColor: "green" },
              headerTitleStyle: { color: "#ffffff" },
              headerTintColor: '#ffffff'
            }}

          />
        </Stack.Navigator>
      </NativeBaseProvider>
    </NavigationContainer>
  )
}

export default App