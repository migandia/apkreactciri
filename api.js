const API = "http://192.168.33.20:1337"
// const API = "http://192.168.33.20:1337"


/** API-INFORMACION ADICIONAL */
export const getInfAdicional = async () => {
  const res = await fetch(API + "/informacion-adicionals")
  return await res.json()
}
/** FIN */

/** API-TIPO DIAGNOSTICO */
export const getDiagnosticos = async () => {
  const res = await fetch(API + "/tipo-diagnosticos")
  return await res.json()
}
/** FIN */

/** API-CUESTIONARIO */
export const getCuestionario = async (itemId) => {
  const res = await fetch(API + "/cuestionarios?id_diagnostico.id_eq=" + itemId)
  return await res.json()
}
/** FIN */

/** API-OBTENER SALA DE CHAT */
export const getSalaChat = async (id) => {
  const res = await fetch(API + "/doctor-chat-pacientes?paciente_eq=" + id + "&estados_chat_eq=2")
  return await res.json()
}
/** FIN */

/** API-GUARDAR-MENSAJES */
export const saveMessages = async (chat, paciente, users_permissions_user, mensaje) => {
  const mes = {
    doctor_chat_paciente: chat,
    paciente: paciente,
    users_permissions_user: users_permissions_user,
    tipo: 'P',
    mensaje: mensaje,
    descargado: '1',
    activo: 'true',
  };
  const res = await fetch(API + "/chats", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(mes)
  });
  return await res.json();
}
/** FIN */

/** API-MENSAGES */
export const getMensages = async (chat, id) => {
  const res = await fetch(API + "/chats?doctor_chat_paciente_eq=" + chat + "&paciente_eq=" + id)
  return await res.json()
}
/** FIN */

/** API-GUARDAR LAS PREGUNTAS RESPONDIDAS */
export const saveRespuestas = async (respuestas, chat, diagnostico) => {
  const respuesta = {
    respuesta: respuestas,
    doctor_chat_paciente: chat,
    tipo_diagnostico: diagnostico
  };
  const res = await fetch(API + "/respuesta-cuestionarios", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(respuesta)
  });
  return await res.json();
}
/** FIN */

/** API-VERIFICA EL CUESTIONARIO RESPONDIDO EN BASE A LA SALA DE CHAT */
export const getRespuestas = async (id) => {
  const res = await fetch(API + "/respuesta-cuestionarios?doctor_chat_paciente_eq=" + id)
  return await res.json()
}
/** FIN */


export const saveTask1 = async (task) => {
  const res = await fetch(API + "/estado-chat-pacientes", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(task),
  });
  return await res.json();
};
export const getTask1 = async (taskId, Estado, activo) => {
  const res = await fetch(API + "/estado-chat-pacientes?paciente_chat_estado_eq=" + taskId + "&estados_chat_eq=" + Estado + "&activo_eq=" + activo);
  return await res.json();
};

export const saveTask = async (newTask) => {
  const res = await fetch(API + "/pacientes", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(newTask),
  });
  return await res.json();
};

export const getTask = async (taskCi, taskMatricula) => {
  const res = await fetch(API + "/pacientes?matricula_eq=" + taskMatricula + "&ci_eq=" + taskCi);
  return await res.json();
};

export const updateTask = async (taskId, newTask) => {
  const res = await fetch(`${API}/${taskId}`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(newTask),
  });
  return res;
};


